# portfolio website

website to showcase my portfolio

# Project setup
## Website Setup
```
cd ./portfolio
```
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
## Firebase Console Setup
```
cd ./firebase
```

```
npm install -g firebase-tools
```
then run:
```
firebase login
```
### Launch firebase development server
```
firebase serve
```
### Deploy the firebase website
```
firebase deploy
```