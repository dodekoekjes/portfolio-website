import Vue from 'vue'
import Router from 'vue-router'

//import NProgress
import Loading from './js/loading.js'

//views
import Home from './views/Home.vue'
import About from './views/About.vue'
import Projects from './views/Projects.vue'
import Error from './views/Error.vue'

//components
import Project from '@/components/projects/Project.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      beforeEnter (to, from, next) {
        Loading.start();
        next();
      }
    },
    {
      path: '/about',
      name: 'about',
      component: About,
      beforeEnter (to, from, next) {
        Loading.start();
        next();
      }
    },
    {
      path: '/projects', component: Projects, name: 'projects',
      beforeEnter (to, from, next) {
        Loading.start();
        //eslint-disable-next-line
        console.log('projects')
        next();
      },
      children: [
        { path: ':projectId', component: Project, name: 'project',
            beforeEnter (to, from, next) {
              //eslint-disable-next-line
              console.log('project')
              if (from.name !== null && from.name !== undefined) {
                if (!from.name.includes('projects')) {
                  Loading.start();
                  
                }
              }
              next(); 
            }
        }
      ]
    },
    {
      path: '*',
      component: Error,
      name: 'error',
      beforeEnter (to, from, next) {
        Loading.start();
        next();
      }
    }
  ]  
})

// router.beforeResolve((to, from, next) => {
//   if (to.name !== null || to.name !== undefined) {
//     // Start the route progress bar.
//     Loading.start()
//   }
//   next()
// })

// router.afterEach(() => {
//   // Complete the animation of the route progress bar.
//   Loading.done();
// })

// export default router;