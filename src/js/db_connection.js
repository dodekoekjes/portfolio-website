import { db } from './firebase.js'

export const getProjects = async (order, where) => {
    var ref = db.collection('projects')
    if (order !== null && order !== undefined) {
        ref = ref.orderBy(order[0], order[1]);
    }
    if (where !== null && where !== undefined) {
        ref = ref.where(where[0], where[1], where[2])
    }

    var projects = await ref.get()
      .then(collection => {
        var projectsIterator = [];
        //eslint-disable-next-line
        console.log('Array:');
        collection.docs.forEach(document => {
            projectsIterator.push({ id: document.id, data: document.data()});
            //eslint-disable-next-line
            console.log('{\n\tprojects/projectId:', document.id);
            //eslint-disable-next-line
            console.log('\tprojects/projectData:', document.data());
            //eslint-disable-next-line
            console.log('}');
        });
        return projectsIterator;
      })
      .catch(error => {
        //eslint-disable-next-line
        console.log('Error getting data', error);
      });
    //eslint-disable-next-line
    console.log('documents', projects)
    return projects;
};

export const getRooms = async (projectId) => {
  //eslint-disable-next-line
  console.log('projectId:', projectId)
  var roomsRef = db.collection('projects').doc(projectId).collection('rooms');
  var rooms = await roomsRef.get()
  .then(collection => {
    var roomsIterator = [];
    //eslint-disable-next-line
    console.log('Array:');
    collection.docs.forEach(document => {
      roomsIterator.push({ id: document.id, data: document.data()});
          //eslint-disable-next-line
          console.log('{\n\trooms/roomId:', document.id);
          //eslint-disable-next-line
          console.log('\trooms/roomData:', document.data());
          //eslint-disable-next-line
          console.log('}');
    });
    return roomsIterator;
  })
  .catch(error => {
    //eslint-disable-next-line
    console.log('Error getting data', error);
  });
  //eslint-disable-next-line
  console.log('rooms:', rooms)
  return rooms
};

export const getMessages = async (projectId, roomId) => {
  //eslint-disable-next-line
  console.log('projectId:', projectId)
  var messagesRef = db.collection('projects').doc(projectId).collection('rooms').doc(roomId).collection('messages').orderBy("timestamp", "asc");
  var messages = await messagesRef.get()
  .then(collection => {
    var messagesIterator = [];
    //eslint-disable-next-line
    console.log('Array:');
    collection.docs.forEach(document => {
      messagesIterator.push({ id: document.id, data: document.data()});
          //eslint-disable-next-line
          console.log('{\n\tmessages/messageId:', document.id);
          //eslint-disable-next-line
          console.log('\tmessages/messageData:', document.data());
          //eslint-disable-next-line
          console.log('}');
    });
    return messagesIterator;
  })
  .catch(error => {
    //eslint-disable-next-line
    console.log('Error getting data', error);
  });
  //eslint-disable-next-line
  console.log('messages:', messages)
  return messages
};

export const getProject = async (projectId, content = true,  body = false, rooms = false, messages = false) => {
    var projectArr = []
    //eslint-disable-next-line
    console.log('Project:');
    if (content) {
        var projectRef = db.collection('projects').doc(projectId);

        var contentObj = await projectRef.get()
        .then(doc => {
            return { id: doc.id, data: doc.data() };
        })
        .catch(error => {
            //eslint-disable-next-line
            console.log('Error getting data', error);
        });
        //eslint-disable-next-line
        console.log('  Content:', contentObj);
        await projectArr.push(contentObj);
    }
    if (body) {
        var bodyRef = db.collection('projects').doc(projectId).collection('body');

        var bodyArr = await bodyRef.get()
        .then(collection => {
            var sectionsIterator = [];
            collection.docs.forEach(document => {
                sectionsIterator.push({ id: document.id, data: document.data()});
            });
            return sectionsIterator;
        })
        .catch(error => {
            //eslint-disable-next-line
            console.log('Error getting data', error);
        });
        //eslint-disable-next-line
        console.log('  body:', bodyArr);
         bodyArr.forEach( elem => {
            projectArr.push(elem);
        });
    }
    if (rooms) {
        var roomsRef = db.collection('projects').doc(projectId).collection('rooms');
        var roomsArr = await roomsRef.get()
        .then(collection => {
            var roomsIterator = [];
            collection.docs.forEach(document => {
                roomsIterator.push({ id: document.id, data: document.data()});
            });
            return roomsIterator;
        })
        .catch(error => {
            //eslint-disable-next-line
            console.log('Error getting data', error);
        });
        //eslint-disable-next-line
        console.log('  rooms:', roomsArr);
        await roomsArr.forEach(async room => {
            projectArr.push(room);
            if (messages) {
                //eslint-disable-next-line
                console.log(room.id)
                var messagesRef = db.collection('projects').doc(projectId).collection('rooms').doc(room.id).collection('messages').orderBy("timestamp", "asc");
                var messagesArr = await messagesRef.get()
                .then(async collection => {
                    var messagesIterator = [];
                    await collection.docs.forEach(async document => {
                        await messagesIterator.push({ id: document.id, data: await document.data()});
                    });
                    return messagesIterator;
                })
                .catch(error => {
                    //eslint-disable-next-line
                    console.log('Error getting data', error);
                });
                //eslint-disable-next-line
                console.log('    messages:', messagesArr)
                await messagesArr.forEach(async message => {
                    await projectArr.push(message);
                });
            }
        });
    }
    return projectArr
};

export const test = async () => {
    var ref = db.collection('projects').doc('portfolio-website');
    var refGet = await ref.get();
    //eslint-disable-next-line
    console.warn('test:\n', 'ref:', ref, '\ndata:', refGet.data());

    ref = ref.collection('body');
    //eslint-disable-next-line
    console.warn('ref:', ref);
    refGet = await ref.get()
    //eslint-disable-next-line
    console.warn('test2:\n', 'ref:', ref, '\ndata:', refGet);

    refGet.forEach(doc => {
        //eslint-disable-next-line
        console.warn('test3:\n', 'id:', doc.id, '\ndata:', doc.data());
    })
};

// test()

export default {
    getProjects,
    getMessages,
    getRooms,
    getProject
};