import firebase from 'firebase'
import 'firebase/firestore'

// Initialize Firebase
var config = {
    apiKey: "AIzaSyAWVPMjIU11KeHsB4Sv4lqyIulawsv1vls",
    authDomain: "auth.tristan-kruijshaar.nl",
    databaseURL: "https://portfolio-615bb.firebaseio.com",
    projectId: "portfolio-615bb",
    storageBucket: "",
    messagingSenderId: "955453061901"
  };

firebase.initializeApp(config);

// firebase auth
firebase.auth().useDeviceLanguage();

export default firebase;
export const db = firebase.firestore();