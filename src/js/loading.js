import Loading from 'nprogress/nprogress.js';

Loading.configure({
    showSpinner: false
});

export default Loading;