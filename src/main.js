import Vue from 'vue'
import App from './App.vue'
import router from './router'

// firebase
import {default as firebase, db} from './js/firebase'

// css
import '@fortawesome/fontawesome-free/css/all.min.css'
import 'nprogress/nprogress.css'
import 'bulma-tooltip/dist/css/bulma-tooltip.min.css'
import './css/overrides.css'

// js
import '@fortawesome/fontawesome-free/js/all.min.js'

// import NProgress
import Loading from './js/loading.js'

Vue.config.productionTip = false

// global filters
// Vue.filter('reverse', function(value) {
//   // slice to make a copy of array, then reverse the copy
//   return value.slice().reverse();
// });

new Vue({
  router,
  data () {
    return {
      firebase: firebase,
      loading: Loading,
      db: db,
      mode: this.getCookie('mode') !== null && this.getCookie('mode') !== undefined ? this.getCookie('mode') : 'pro'
    }
  },
  methods: {
    getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    }
  },
  render: h => h(App)
}).$mount('#app')
